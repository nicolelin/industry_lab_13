package ictgradschool.industry.lab13.ex03;

import ictgradschool.Keyboard;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.IllegalFormatException;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * TODO Create a multi-threaded program to calculate PI using the Monte Carlo method.
 */
public class ExerciseThreeMultiThreaded extends ExerciseThreeSingleThreaded {

    long numSamples;
    long numThreads;

    protected void start() {

        System.out.println("Enter the number of samples to use for approximation:");
        System.out.print("> ");

        try {
            numSamples = Long.parseLong(Keyboard.readInput());
        } catch (IllegalFormatException e) {
            System.out.println("Wrong format");
        }

        System.out.println("Number of samples: " + numSamples);

        System.out.println("Enter the number of threads to use for approximation:");
        System.out.print("> ");

        try {
            numThreads = Long.parseLong(Keyboard.readInput());
        } catch (IllegalFormatException e) {
            System.out.println("Wrong format");
        }

        System.out.println("Number of threads: " + numThreads);

        System.out.println("Estimating PI...");
        long startTime = System.currentTimeMillis();

        double estimatedPi = estimatePI(numSamples);

        long endTime = System.currentTimeMillis();
        long timeInMillis = endTime - startTime;

        double difference = Math.abs(estimatedPi - Math.PI);
        double differencePercent = 100.0 * difference / Math.PI;
        NumberFormat format = new DecimalFormat("#.####");

        System.out.println("Estimate of PI: " + estimatedPi);
        System.out.println("Estimate is within " + format.format(differencePercent) + "% of Math.PI");
        System.out.println("Estimation took " + timeInMillis + " milliseconds");

    }

    /**
     * Estimates PI using a multi-threaded Monte Carlo method.
     */
    @Override
    protected double estimatePI(long numSamples) {

        // TODO Implement this.

        double avgOfAllPiSamples = 0.0; // Store final value returned by joined threads

        List<Double> listOfPi = new ArrayList<>();
        List<Thread> listOfThreads = new ArrayList<>();

        Runnable newRunnable = new Runnable() {
            @Override
            public void run() {
                ThreadLocalRandom tlr = ThreadLocalRandom.current(); // Math.Random are thread-safe, we need to do random samples that are not thread-safe so we can call it multiple times in different threads - we want access in parallel

                long numInsideCircle = 0;

                for (long i = 0; i < numSamples; i++) {

                    double x = tlr.nextDouble();
                    double y = tlr.nextDouble();

                    if (Math.pow(x, 2.0) + Math.pow(y, 2.0) < 1.0) {
                        numInsideCircle++;
                    }

                }

                double estimatedPi = 4.0 * (double) numInsideCircle / (double) numSamples;
                listOfPi.add(estimatedPi);

            }
        };

        for (int i = 0; i < numThreads; i++) {
//            System.out.println("New thread");
            Thread newThread = new Thread(newRunnable, "Subsample " + (i + 1)); // Started thread 1, increment each time we start new thread
            newThread.start();
            listOfThreads.add(newThread);
        }

        for (Thread newThread : listOfThreads) {
            try {
                newThread.join();
                System.out.println(newThread.getName() + " finished running");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        Double sumOfThreads = 0.0;

        for (Double pi : listOfPi) {
            sumOfThreads += pi;
        }

        avgOfAllPiSamples = sumOfThreads / numThreads;

        System.out.println("Value: " + avgOfAllPiSamples);
        return avgOfAllPiSamples;
    }

    /**
     * Program entry point.
     */
    public static void main(String[] args) {
        new ExerciseThreeMultiThreaded().start();
    }
}
