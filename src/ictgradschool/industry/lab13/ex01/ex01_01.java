package ictgradschool.industry.lab13.ex01;

/**
 * Created by ylin183 on 5/05/2017.
 */
public class ex01_01 {

    public void start() {
        Runnable myRunnable = new myRunnable();
    }

    public class myRunnable implements Runnable {

        @Override
        public void run() {
            for (int i = 0; i < 1000000; i++) {
                System.out.println(i);
            }
        }
    }

    public static void main(String[] args) {
        new ex01_01().start();
    }
}
