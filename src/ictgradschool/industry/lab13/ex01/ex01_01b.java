package ictgradschool.industry.lab13.ex01;

/**
 * Created by ylin183 on 5/05/2017.
 */
public class ex01_01b {

    public void start() {
        Runnable myRunnable = new myRunnable();
        Thread myThread = new Thread(myRunnable);
        myThread.start();
    }

    public class myRunnable implements Runnable {

        @Override
        public void run() {
            for (int i = 0; i < 1000000 && !Thread.currentThread().isInterrupted(); i++) {
                System.out.println(i);
            }
        }
    }

    public static void main(String[] args) {
        new ex01_01b().start();
    }
}
