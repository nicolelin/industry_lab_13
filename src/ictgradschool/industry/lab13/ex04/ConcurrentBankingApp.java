package ictgradschool.industry.lab13.ex04;

import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * Created by ylin183 on 30/05/2017.
 */
public class ConcurrentBankingApp {

    public static void main(String[] args) {

        // Define BlockingQueue with buffersize (capacity)
        final BlockingQueue<Transaction> blockQueue = new ArrayBlockingQueue<Transaction>(10);

        // Generate list of transactions
        List<Transaction> bankTransactions = TransactionGenerator.readDataFile();

        // Make new bank account to mutate
        BankAccount bankAccount = new BankAccount();

        // Producer
        Producer p = new Producer(blockQueue, bankAccount, bankTransactions);
        Thread producer = new Thread(p);
        producer.start();

        // Consumer
        Consumer c1 = new Consumer(blockQueue, bankAccount);
        Consumer c2 = new Consumer(blockQueue, bankAccount);
        Thread consumer1 = new Thread(c1);
        Thread consumer2 = new Thread(c2);
        consumer1.start();
        consumer2.start();

        // Join / merge to main thread
        try {
            producer.join();
            System.out.println("Producer " + producer.getName() + " has finished.");

            // Notify consumers that producer has stopped
            // Send interrupt message to both consumers
            consumer1.interrupt();
            System.out.println(consumer1.getName() + " is interrupted");
            consumer2.interrupt();
            System.out.println(consumer2.getName() + " is interrupted");

            // Join / merge to main thread
            consumer1.join();
            System.out.println(consumer1.getName() + " is finished");
            consumer2.join();
            System.out.println(consumer2.getName() + " is finished");

        } catch (InterruptedException e) {
            System.out.println(e.getMessage());
        }

        System.out.println("Final bank balance: " + bankAccount.getFormattedBalance());

    }
}
