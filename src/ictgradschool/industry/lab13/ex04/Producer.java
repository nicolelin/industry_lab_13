package ictgradschool.industry.lab13.ex04;

import java.util.List;
import java.util.concurrent.BlockingQueue;

/**
 * Created by ylin183 on 30/05/2017.
 */
public class Producer implements Runnable {

    // Define variables
    private BlockingQueue<Transaction> queue;
    private BankAccount bankAccount;
    private List<Transaction> transactionList;

    // Constructor
    public Producer(BlockingQueue<Transaction> queue, BankAccount bankAccount, List<Transaction> transactionList) {
        this.queue = queue;
        this.bankAccount = bankAccount;
        this.transactionList = transactionList;
    }

    @Override
    public void run() {

        for (int i = 0; i < transactionList.size(); i++) {
            try {
                if (queue.remainingCapacity() > 0) {
                    System.out.println("Added transaction " + i + " to queue which is " + queue.size() + " long.");
                }
                queue.put(transactionList.get(i));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
