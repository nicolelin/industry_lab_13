package ictgradschool.industry.lab13.ex04;

import java.util.concurrent.BlockingQueue;

/**
 * Created by ylin183 on 30/05/2017.
 */
public class Consumer implements Runnable {

    // Define variables
    private BlockingQueue<Transaction> queue;
    private BankAccount bankAccount;

    // Constructor
    public Consumer(BlockingQueue<Transaction> queue, BankAccount bankAccount) {
        this.queue = queue;
        this.bankAccount = bankAccount;
    }

    @Override
    public void run() {

        try {
            while (!Thread.currentThread().isInterrupted()) {
                while (queue.peek() != null) {
                    Transaction transaction = queue.take();

                    // Check if it's a deposit or withdrawal
                    switch (transaction._type) {
                        case Deposit:
                            bankAccount.deposit(transaction._amountInCents);
                            System.out.println(bankAccount.getBalance());
                            break;
                        case Withdraw:
                            bankAccount.withdraw(transaction._amountInCents);
                            System.out.println(bankAccount.getBalance());
                            break;

                    }

                    // Print name of thread to check which executed the transaction
                    System.out.println(Thread.currentThread().getName() + " : " + transaction._type + " : " + transaction._amountInCents);

                }
            }

        } catch (InterruptedException e) {
            System.out.println(e.getMessage());

            // When the producer has grabbed everything and consumer has consumed everything, it will still tell consumer it's finished, but need to make sure the consumer will do one more clean out

            while (queue.peek() != null) {
                Transaction transaction = queue.poll();

                // Check if it's a deposit or withdrawal
                switch (transaction._type) {
                    case Deposit:
                        bankAccount.deposit(transaction._amountInCents);
                        System.out.println(bankAccount.getBalance());
                        break;
                    case Withdraw:
                        bankAccount.withdraw(transaction._amountInCents);
                        System.out.println(bankAccount.getBalance());
                        break;

                }

                // Print name of thread to check which executed the transaction
                System.out.println(Thread.currentThread().getName() + " : " + transaction._type + " : " + transaction._amountInCents);

            }
        }
    }
}
