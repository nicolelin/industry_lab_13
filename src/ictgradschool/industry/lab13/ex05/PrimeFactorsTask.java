package ictgradschool.industry.lab13.ex05;

import ictgradschool.Keyboard;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ylin183 on 30/05/2017.
 */
public class PrimeFactorsTask implements Runnable {

    long n;

    List<Long> primeFactors;

    TaskState taskStates;

    // Assign value n into constructor
    public PrimeFactorsTask() {

        System.out.println("Please enter a long number: ");
        n = Long.parseLong(Keyboard.readInput());

//        String testNum = "9201111169755555649";
//        n = Long.parseLong(testNum);

        primeFactors = new ArrayList<>();
    }

    private long n() {
        return n;
    }

    private List<Long> getPrimeFactors() throws IllegalStateException {
        return primeFactors;
    }

    private TaskState getTaskStates() {
        return taskStates;
    }


    @Override
    public void run() {

        taskStates = TaskState.Initialized;

        // for each potential factor
        for (long factor = 2; factor * factor <= n; factor++) {

            // if factor is a factor of n, repeatedly divide it out
            if (taskStates == TaskState.Aborted) {
                System.out.println("Stopping thread...");
                return;
            }
            while (n % factor == 0) {
                if (taskStates == TaskState.Aborted) {
                    System.out.println("Stopping thread...");
                    return;
                }
                System.out.print(factor + " ");
                // Add n into primeFactors List:
                primeFactors.add(factor);
                n = n / factor;
            }
        }


        // if biggest factor occurs only once, n > 1
        if (n > 1) {
            // Add n into primeFactors List:
            primeFactors.add(n);
            System.out.println("n: " + n);
        }

        taskStates = TaskState.Completed;

    }
}
