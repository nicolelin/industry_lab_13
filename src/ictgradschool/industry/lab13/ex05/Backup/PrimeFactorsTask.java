package ictgradschool.industry.lab13.ex05.Backup;

import ictgradschool.Keyboard;
import ictgradschool.industry.lab13.ex05.TaskState;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ylin183 on 30/05/2017.
 */
public class PrimeFactorsTask implements Runnable {

    long n;

    List<Long> primeFactors;

    TaskState taskStates;

    // Assign value n into constructor
    public PrimeFactorsTask() {

        System.out.println("Please enter a long number: ");
        n = Long.parseLong(Keyboard.readInput());

        primeFactors = new ArrayList<>();
//        taskStates = TaskState.

    }

    private long n() {

        return n;
    }

    private List<Long> getPrimeFactors() throws IllegalStateException {
        return primeFactors;
    }

    private TaskState getTaskStates() {
        return taskStates;
    }


    @Override
    public void run() {

        // for each potential factor

        for (long factor = 2; factor * factor <= n; factor++) {

            // if factor is a factor of n, repeatedly divide it out
            while (n % factor == 0) {

                System.out.print(factor + " ");
                primeFactors.add(factor);
                n = n / factor;
            }
        }

        // Add n into primeFactors List:

        // if biggest factor occurs only once, n > 1
        if (n > 1 ) {
            primeFactors.add(n);
            System.out.println("n: " + n);
        }

    }
}
