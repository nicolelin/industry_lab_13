package ictgradschool.industry.lab13.ex05;

import ictgradschool.Keyboard;

/**
 * Created by ylin183 on 30/05/2017.
 */
public class ExerciseFive {

    public static void main(String[] args) { // container/main thread (1st thread)
        PrimeFactorsTask pft = new PrimeFactorsTask(); // pft is computation thread (2nd thread)

        // Threads are independent of each other
        // New thread - abort/interrupting thread (3rd thread)
        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("Do you want to stop the computation thread?");

                if (Keyboard.readInput().contains("yes")) {
                    pft.taskStates = TaskState.Aborted;
                    System.out.println("Setting status to aborted...");
                }

            }
        });
        t1.setDaemon(true); // if all other threads finish, t1 will finish

        // Run threads
        t1.start(); // run abort thread
        pft.run(); // run computation thread

        System.out.println("Abort thread state: " + t1.getState());
//        try {
//            t1.join(); // t1 join is waiting for t1 to finish
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        System.out.println("Is abort thread running? " + t1.getState());

    }
}
