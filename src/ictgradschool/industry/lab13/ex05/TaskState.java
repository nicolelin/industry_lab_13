package ictgradschool.industry.lab13.ex05;

/**
 * Created by ylin183 on 30/05/2017.
 */
public enum TaskState {
    Initialized, Completed, Aborted
}
